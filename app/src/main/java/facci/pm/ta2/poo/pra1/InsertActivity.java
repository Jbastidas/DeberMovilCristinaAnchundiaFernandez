package facci.pm.ta2.poo.pra1;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.SaveCallback;

public class InsertActivity extends AppCompatActivity {

    Button buttonCamara, buttonInsert;
    EditText txtNombre;
    EditText txtDescripcion;
    EditText txtPrecio;
    Uri selectedImage;

    ImageView imageViewFoto;
    private static final int SELECT_FILE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);

        buttonCamara = (Button) findViewById(R.id.buttonCamara);
        buttonInsert = (Button) findViewById(R.id.ButtonInsert);
        imageViewFoto = (ImageView) findViewById(R.id.imageViewFoto);
        txtDescripcion = findViewById(R.id.txtDescripcion);
        txtNombre = findViewById(R.id.txtNombre);
        txtPrecio= findViewById(R.id.txtPrecio);


        buttonCamara.setOnClickListener(new View.OnClickListener() {
            @Override
                public void onClick(View v) {
                abrirGaleria();
            }
        });
        buttonInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final DataObject objeto = new DataObject("item");
                imageViewFoto.buildDrawingCache();
                Bitmap bmap = imageViewFoto.getDrawingCache();
                objeto.put("image",bmap);
                String nomb = String.valueOf(txtNombre.getText().toString());
                objeto.put("name",nomb );
                String prec = String.valueOf(txtPrecio.getText().toString());
                objeto.put("price", prec);
                String descrip = String.valueOf(txtDescripcion.getText().toString());
                objeto.put("descripction",descrip);


                objeto.saveInBackground(new SaveCallback<DataObject>() {

                    @Override
                    public void done(DataObject object, DataException e) {
                        Toast.makeText(getApplicationContext(),"Inserción Correcta", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(InsertActivity.this, ResultsActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
            }
        });
    }
    public void abrirGaleria(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(
                Intent.createChooser(intent, "Seleccione una imagen"),
                SELECT_FILE);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(resultCode == RESULT_OK && requestCode == SELECT_FILE){
            selectedImage = data.getData();
            imageViewFoto.setImageURI(selectedImage);
        }
    }
//    protected void onActivityResult(int requestCode, int resultCode,
//                                    Intent imageReturnedIntent) {
//        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
//        String filePath = null;
//        switch (requestCode) {
//            case SELECT_FILE:
//                if (resultCode == RESULT_OK) {
//                    selectedImage = imageReturnedIntent.getData();
//                    String selectedPath=selectedImage.getPath();
//                    if (requestCode == SELECT_FILE) {
//
//                        if (selectedPath != null) {
//                            InputStream imageStream = null;
//                            try {
//                                imageStream = getContentResolver().openInputStream(selectedImage);
//                            } catch (FileNotFoundException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    }
//                }
//                break;
//        }
//    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //Esconde el teclado cuando se da click en cualquier parte de la Activity que no sea un EditText
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),0);
        return super.onTouchEvent(event);
    }
}
